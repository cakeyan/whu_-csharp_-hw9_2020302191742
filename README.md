Week12_HW9

- I1_Words

  > **Notice**: The WinForm must be runned in the **X64** environment instead of AnyCPU.
  
  > Implement a WinForm, which can help reciting words. You can press `Enter` key to compare the word you input with the answer.