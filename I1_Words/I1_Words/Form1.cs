﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace I1_Words
{
    // 手打了15个单词作为示例
    // 请将Any CPU改成X64运行，否则会报错！
    public partial class Form1 : Form
    {
        List<string> ChiList = new List<string>();
        List<string> EngList = new List<string>();
        int i = 0;
        public Form1()
        {
            InitializeComponent();
            SQLiteConnectionStringBuilder connection = new SQLiteConnectionStringBuilder();
            connection.DataSource = Application.StartupPath + @"\words.db3";
            // 请将Any CPU改成X64运行，否则会报错！
            SQLiteConnection db = new SQLiteConnection(connection.ToString()); //连接数据库
            db.Open();
            SQLiteCommand cmd = new SQLiteCommand(@"SELECT chi FROM words", db);
            SQLiteCommand cmd2 = new SQLiteCommand(@"SELECT eng FROM words", db);
            var rd1 = cmd.ExecuteReader();
            var rd2 = cmd2.ExecuteReader();
            while (rd1.Read() && rd2.Read())
            {
                ChiList.Add(rd1.GetString(0));
                EngList.Add(rd2.GetString(0));
            }
            db.Close();
            label1.Text = ChiList[i];
            label3.Text = "";
            label5.Text = "";
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            string word = textBox1.Text;
            if (word == EngList[i])
            {
                label3.Text = "正确";
                label5.Text = "";
            }
            else
            {
                label3.Text = "错误";
                label5.Text = EngList[i].ToString();

            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (i < 14)
            {
                i += 1;
                label1.Text = ChiList[i];
                label3.Text = "";
                label5.Text = "";
                textBox1.Text = "";
            }
            else
            {
                i = 0;
                label1.Text = ChiList[i];
                label3.Text = "";
                label5.Text = "";
                textBox1.Text = "";
            }
        }
    }
}
